/******************************************************************************
 File: ncaa_apr.sas
 Author: ProcRun
 Twitter: ProcRun
 Web: http://www.procrun.com

 Notes:

 The NCAA APR's are based on the information available on the NCAA  website 
 (http://web1.ncaa.org/maps/aprRelease.jsp).  The table was c & p into an excel 
 spreadsheet and brought into SAS. 

 The program will calculate the moving two year and four year averages for each
 school in the NCAA database.  The APR is important due to the following 
 changes in NCAA legislation as of 2011-10-27.

 "The new postseason eligibility structure will take effect in the 2012-13 
  academic year, with a two-year implementation window before the benchmark 
  moves from 900 to 930. For access to postseason competition in 2012-13 and 
  2013-14, teams must achieve a 900 multi-year APR or a 930 average in the most 
  recent two years to be eligible.

  In 2014-15, teams that don’t achieve the 930 benchmark for their four-year 
  APR or at least a 940 average for the most recent two years will be 
  ineligible for postseason competition."
******************************************************************************/
* Point path to your directory for the input / output files;
%let path = 'your path here';

options mprint;

proc import datafile = "&path.\apr.xls"
            out = apr (rename = (f2 = school
					   f4 = year
					   f5 = apr))
	dbms = excel
	replace;
run;

data apr; 
 set apr;
 where f1 = 'Football';
run;

proc sql noprint;
 select
  distinct year,
  count(distinct year)
into 
  :year1 - :year9999, :count
 from
  apr;
quit;

%let count = &count;


%macro cleanYear();

 %do i = 1 %to &count;
 %let j = %eval(&i. + 3);
 %let k = data_0&j.;

 data &k.;
  set apr;
  where year = "&&year&i.";
 run;

 proc transpose data = &k. out = wide_&k. (keep = school col1 
										   rename = (col1 = year_&k.));
  by school;
  var apr;
 run;

 proc sort data = wide_&k; by school; run;

 %end;
%mend;
%cleanYear();

data all_years;
 merge wide_04
	   wide_05
       wide_06
       wide_07
       wide_08
       wide_09;
 by school;

 array twoyear[*] twoyear04-twoyear09;
 array fouryear[*] fouryear04-fouryear09;
 array yr[*] year_:;

 do i = 2 to dim(twoyear);
 	j = i - 1;
	twoyear[i] = (yr[j] + yr[i]) / 2;
 end; 

 drop i j;

 do q = 4 to dim(fouryear);
   r = q - 3; s = q - 2; t = q - 1;
   fouryear[q] = (yr[r] + yr[s] + yr[t] + yr[q]) / 4;
 end;
   
 drop q r s t;
run;

proc export data=all_years
   outfile="&path.\ncaa_apr.xls"
   dbms=excel
   replace;
run;
